import React from "react";
import { theme } from "../config/theme.js";
import GoodsItem from "../config/GoodsItem.js";
import { ThemeProvider } from "@emotion/react";
import { Container, Grid } from "@mui/material";
import logo from "../Media/sran.svg";
import logo2 from "../Media/sran2.svg";
import logo3 from "../Media/sran3.svg";
import logo4 from "../Media/sran4.svg";

const goods = [
  {
    id: 1,
    name: "Утиль немецкий",
    price: "600 000Р",
    media: logo,
  },
  {
    id: 2,
    name: "Утиль японский",
    price: "400 000Р",
    media: logo2,
  },
  {
    id: 3,
    name: "Утиль отечественный",
    price: "300 000Р",
    media: logo3,
  },
  {
    id: 4,
    name: "Утиль британский",
    price: "1 000 000Р",
    media: logo4,
  },
  {
    id: 4,
    name: "Утиль британский",
    price: "1 000 000Р",
    media: logo,
  },
  {
    id: 4,
    name: "Утиль британский",
    price: "1 000 000Р",
    media: logo2,
  },
  {
    id: 4,
    name: "Утиль британский",
    price: "1 000 000Р",
    media: logo3,
  },
  {
    id: 4,
    name: "Утиль британский",
    price: "1 000 000Р",
    media: logo4,
  },
];

const GoodsList = () => {
  return (
    <ThemeProvider theme={theme}>
      <Container variant="Container">
        <Grid container spacing={5}>
          {goods.map((item) => (
            <GoodsItem {...item} />
          ))}
        </Grid>
      </Container>
    </ThemeProvider>
  );
};

export default GoodsList;
