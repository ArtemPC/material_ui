import { Typography, Button, Box, Card, Grid, CardMedia } from "@mui/material";

const GoodsItem = ({ name, price, media }) => {
  return (
    <Grid item md="3">
      <Card variant="saleCard" sx={{ width: 200, height: 250 }}>
        <CardMedia component="img" image={media} alt="logo" />
        <Box mt={"15px"}>
          <Typography variant="h4" ml={"12px"} mb={"5px"}>
            {name}
          </Typography>
          <Box
            sx={{
              margin: "auto",
              width: "180px",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Typography variant="price">{price}</Typography>
            <Button variant="saleBtn" sx={{ width: 80, height: 18 }}>
              <Typography variant="button">Купить</Typography>
            </Button>
          </Box>
        </Box>
      </Card>
    </Grid>
  );
};
export default GoodsItem;
