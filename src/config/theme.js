import { createTheme } from "@mui/material";

const colors = {
  whiteGray: "#70977B",
  black: "#000000",
  white: "#FFFFFF",
};

const palette = createTheme({
  palette: {
    primary: {
      main: colors.whiteGray,
      contrastText: colors.black,
    },
    secondary: {
      main: colors.white,
    },
  },
});
const breakpoints = createTheme({});

export const theme = createTheme(palette, {
  typography: {
    fontFamily: "arial",
    h4: {
      color: palette.palette.secondary.main,
      width: "160px",
      fontSize: 20,
      fontWeight: 600,
    },
    button: {
      fontSize: 12,
      fontWeight: 200,
      color: palette.palette.primary.contrastText,
      textTransform: "uppercase",
    },
    price: {
      fontSize: 16,
      fontWeight: 400,
      color: palette.palette.secondary.main,
      textTransform: "uppercase",
    },
    // TODO: сделать вариант button и price
    // Components: сделать вариант кнопки saleBtn, и вариант карточки saleCard
  },
  components: {
    MuiCard: {
      variants: [
        {
          props: { variant: "saleCard" },
          style: {
            backgroundColor: "#008000",
            borderRadius: "15px",
            [breakpoints.breakpoints.down("md")]: {
              backgroundColor: palette.palette.primary.contrastText,
            },
          },
        },
      ],
    },

    MuiButton: {
      variants: [
        {
          props: { variant: "saleBtn" },
          style: {
            backgroundColor: palette.palette.secondary.main,
            borderRadius: "5px",
          },
        },
      ],
    },
    Container: {
      styleOverrides: {
        root: {
          Width: "900px",
          position: "fixed",
        },
      },
    },
  },
});
