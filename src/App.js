import GoodsList from "../src/config/GoodsList.js";

function App() {
  return <GoodsList />;
}

export default App;
